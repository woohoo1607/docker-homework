export const unique = (arr) => {
  return Array.from(new Set(arr));
}

export const getDate = () => {
  let newDate = new Date();
  return newDate.toISOString()
}

export const sortByTime = (arr) => {
  arr.sort((a, b) => a.createdAt > b.createdAt ? 1 : -1);
}
