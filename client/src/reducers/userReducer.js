export const SET_USER = 'SET_USER';
export const SET_IS_FETCHING = 'SET_IS_FETCHING';
export const SET_IS_AUTH = 'SET_IS_AUTH';
export const LOGIN_USER = 'LOGIN_USER';
const IS_USER_ERROR = 'IS_USER_ERROR';
const MSG_USER_ERROR = 'MSG_USER_ERROR';
export const CHANGE_USER_ERROR = 'CHANGE_USER_ERROR';
const SET_USERS_LIST = 'SET_USERS_LIST';
const SET_CURRENT_USER = 'SET_CURRENT_USER';
export const REQUEST_USERS_LIST = 'REQUEST_USERS_LIST';
export const REQUEST_CURRENT_USER = 'REQUEST_CURRENT_USER';
export const REQUEST_EDIT_USER = 'REQUEST_EDIT_USER';
export const REQUEST_DELETE_USER = 'REQUEST_DELETE_USER';
export const REQUEST_CREATE_USER = 'REQUEST_CREATE_USER';

let initialState = {
  user: {},
  usersList: [],
  currentUser: {},
  isFetching: false,
  isAuth: false,
  isUserError: false,
  msgUserError: '',
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_USER:
    {
      return {
        ...state,
        user: {...action.user}
      };
    }
    case SET_USERS_LIST:
    {
      return {
        ...state,
        usersList: [...action.usersList]
      };
    }
    case SET_CURRENT_USER:
    {
      return {
        ...state,
        currentUser: {...action.currentUser}
      };
    }
    case SET_IS_FETCHING:
    {
      return {
        ...state,
        isFetching: action.isFetching
      };
    }
    case SET_IS_AUTH:
    {
      return {
        ...state,
        isAuth: action.isAuth
      };
    }
    case IS_USER_ERROR:
    {
      return {
        ...state,
        isUserError: action.isUserError
      };
    }
    case MSG_USER_ERROR:
    {
      return {
        ...state,
        msgUserError: action.msgUserError
      };
    }
    default:
      return state;
  }
};

export const setUser = (user) =>
    ({type: SET_USER, user: user});

export const setUsersLists = (usersList) =>
    ({type: SET_USERS_LIST, usersList: usersList});

export const setCurrentUser = (currentUser) =>
    ({type: SET_CURRENT_USER, currentUser: currentUser});

export const setIsFetchingUser = (isFetching) =>
    ({type: SET_IS_FETCHING, isFetching: isFetching});

export const setIsAuth = (isAuth) =>
    ({type: SET_IS_AUTH, isAuth: isAuth});

export const setIsUserError = (isUserError) =>
    ({type: IS_USER_ERROR, isUserError: isUserError});

export const setMsgUserError = (msgUserError) =>
    ({type: MSG_USER_ERROR, msgUserError: msgUserError});

export const loginUser = (data) =>
  ({type: LOGIN_USER, data});

export const requestCurrentUser = (id) =>
    ({type: REQUEST_CURRENT_USER, id});

export const requestUsersList = () =>
    ({type: REQUEST_USERS_LIST});

export const requestEditUser = (user, meta) =>
    ({type: REQUEST_EDIT_USER, user, meta});

export const requestDeleteUser = (userId) =>
    ({type: REQUEST_DELETE_USER, userId});

export const requestCreateUser = (user, meta) =>
    ({type: REQUEST_CREATE_USER, user, meta});

export default userReducer;
