import React from 'react';
import {Route, Switch} from "react-router-dom";

import './App.css';
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";
import ChatContainer from "./components/Chat/ChatContainer";
import LoginContainer from "./components/Login/LoginContainer";
import UsersListContainer from "./components/UsersList/UsersListContainer";
import EditUserContainer from "./components/EditUser/EditUserContainer";
import EditMessageContainer from "./components/EditMessage/EditMessageContainer";
import PopUp from "./components/PopUp/PopUp";

function App() {
  return (
    <div className="App">
      <Header />
      <Switch>
        <Route exact path='/login' component={LoginContainer} />
        <Route exact path='/' component={ChatContainer} />
        <Route exact path='/users' component={UsersListContainer} />
        <Route exact path='/users/:id' component={EditUserContainer} />
        <Route exact path='/edit/:id/:message' component={EditMessageContainer} />
        <Route component={LoginContainer} />
      </Switch>
      <PopUp />
      <Footer />
    </div>
  );
}

export default App;
