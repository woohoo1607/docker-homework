import React from 'react';

import './styles.css';

const Modal = ({msg, changeInput, saveMsg, cancelEdit}) => {
  return (
      <div className='modal'>
        <div className='modalHeader'>
          <h3>Edit message</h3>
        </div>
        <div className='modalBody'>
          <textarea value={msg} onChange={changeInput}/>
        </div>
        <div className='modalFooter'>
          <button onClick={saveMsg}>Ok</button>
          <button onClick={cancelEdit}>Cancel</button>
        </div>
      </div>
  )
};

export default Modal;
