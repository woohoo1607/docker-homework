import React from 'react';
import {NavLink} from "react-router-dom";

import './styles.css';
import Preloader from "../Preloader/Preloader";

const UsersList = ({usersList, isFetching, deleteUser}) => {
  return (
      <div className='usersList'>
        <div className='center'>
          <NavLink to={`/users/new`} className='listButton createBtn'>Create User</NavLink>
          <h2>Users list</h2>
          {!isFetching && <ul>
            {usersList.map(user => {
              return (
                  <li key={user.userId}>
                    <h4>{user.user}</h4>
                    <NavLink to={`/users/${user.userId}`} className='listButton editBtn'>Edit</NavLink>
                    <button className='listButton deleteBtn' onClick={deleteUser(user.userId)}>Delete</button>
                  </li>
              )
            })}
          </ul>}
          {isFetching && <Preloader />}
        </div>
      </div>
  )
};

export default UsersList;
