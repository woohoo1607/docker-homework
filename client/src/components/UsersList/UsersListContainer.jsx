import React, {useEffect} from "react";
import {connect} from "react-redux";
import UsersList from "./UsersList";
import {withRouter} from "react-router-dom";
import {requestDeleteUser, requestUsersList} from "../../reducers/userReducer";

const UsersListContainer = (props) => {
  useEffect(() => {
    props.requestUsersList()
  },[])

  const deleteUser = (userId) => () => {
    props.requestDeleteUser(userId);
  }
  if (!props.isAuth) {
    props.history.push('/login')
  }
  if (props.isAuth && !props.user.isAdmin) {
    props.history.push('/')
  }
  return (
      <UsersList usersList={props.usersList}
                 isFetching={props.isFetching}
                 deleteUser={deleteUser}
      />
  )
};

let mapStateToProps = (state) => {
  return {
    user: state.user.user,
    isFetching: state.user.isFetching,
    isAuth: state.user.isAuth,
    usersList: state.user.usersList,
  }
};

export default connect(mapStateToProps, {requestUsersList, requestDeleteUser})(withRouter(UsersListContainer))
