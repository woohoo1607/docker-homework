const express = require('express');
const router = express.Router();

const userController = require("../controllers/usersController");

router.get('/users', userController.index);
router.get('/users/:id', userController.read);
router.post('/users', userController.write);
router.put('/users', userController.update);
router.delete('/users/:id', userController.delete);


module.exports = router;
